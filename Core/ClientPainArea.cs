//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientPainArea
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int PainId { get; set; }
    
        public virtual PainArea PainArea { get; set; }
        public virtual ClientBooking ClientBooking { get; set; }
    }
}
