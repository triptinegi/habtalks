﻿CREATE TABLE [dbo].[PainArea] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [Pain_Area]  VARCHAR (50) NOT NULL,
    [IsSelected] BIT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

