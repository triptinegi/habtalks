﻿CREATE TABLE [dbo].[ClientAppointment] (
    [Id]              INT      IDENTITY (1, 1) NOT NULL,
    [ClientId]        INT      NOT NULL,
    [AppointmentDate] DATETIME NOT NULL,
    [Time]            TIME (7) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClientBooking_Appointment] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientBooking] ([Id])
);

