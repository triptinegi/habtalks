﻿CREATE TABLE [dbo].[Enquiry] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (100) NOT NULL,
    [email]       VARCHAR (35)  NOT NULL,
    [Enquiry]     VARCHAR (500) NOT NULL,
    [EnquiryFrom] VARCHAR (100) NULL,
    [Comment]     VARCHAR (500) NULL,
    [createdDate] DATETIME      NOT NULL,
    CONSTRAINT [PK__Enquiry] PRIMARY KEY CLUSTERED ([id] ASC)
);

