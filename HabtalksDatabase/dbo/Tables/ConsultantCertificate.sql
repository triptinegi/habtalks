﻿CREATE TABLE [dbo].[ConsultantCertificate] (
    [id]             INT             IDENTITY (1, 1) NOT NULL,
    [ConsultantId]   INT             NOT NULL,
    [FileName]       VARCHAR (50)    NOT NULL,
    [ContentType]    VARCHAR (50)    NOT NULL,
    [FileData]       VARBINARY (MAX) NOT NULL,
    [createdDate]    DATETIME        NOT NULL,
    [createdBy]      VARCHAR (50)    NOT NULL,
    [modifiedDate]   DATETIME        NOT NULL,
    [modifiedBy]     VARCHAR (50)    NOT NULL,
    [RegistrationNo] VARCHAR (25)    NOT NULL,
    [IDProof]        VARCHAR (35)    NOT NULL,
    [IDProofContent] VARBINARY (MAX) NOT NULL,
    [ConsultantGST]  VARCHAR (35)    NOT NULL,
    CONSTRAINT [PK_ConsultantCertificate] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Consultant_ConsultantCertificate] FOREIGN KEY ([ConsultantId]) REFERENCES [dbo].[Consultant] ([id])
);

