﻿CREATE TABLE [dbo].[ClientReview] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [ConsultantId] INT           NOT NULL,
    [ClientId]     INT           NOT NULL,
    [Rating]       INT           NOT NULL,
    [Experience]   VARCHAR (500) NOT NULL,
    [ConsultFor]   VARCHAR (50)  NOT NULL,
    [BodyPart]     VARCHAR (50)  NOT NULL,
    [HeardAboutUs] VARCHAR (50)  NOT NULL,
    [Suggestion]   VARCHAR (200) NOT NULL,
    [ReferUs]      BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Client_ClientReview] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Customer] ([id]),
    CONSTRAINT [FK_Consultant_ClientReview] FOREIGN KEY ([ConsultantId]) REFERENCES [dbo].[Consultant] ([id])
);

