﻿CREATE TABLE [dbo].[ClientPayment] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [ClientId]            INT           NOT NULL,
    [PaymentDone]         BIT           NOT NULL,
    [razorpay_payment_id] VARCHAR (50)  NOT NULL,
    [refund_payment_id]   VARCHAR (50)  NOT NULL,
    [razorpay_signature]  VARCHAR (500) NOT NULL,
    [SelectedPlan]        INT           NOT NULL,
    CONSTRAINT [PK__ClientPa__3214EC074ED92E42] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClientBooking_Payment] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientBooking] ([Id]),
    CONSTRAINT [FK_ClientBooking_RehabPlans] FOREIGN KEY ([SelectedPlan]) REFERENCES [dbo].[RehabPlans] ([Id])
);

