﻿CREATE TABLE [dbo].[ConsultantMajor] (
    [id]              INT          IDENTITY (1, 1) NOT NULL,
    [ConsultantMajor] VARCHAR (50) NOT NULL,
    [modifiedDate]    DATETIME     NOT NULL,
    [modifiedBy]      VARCHAR (50) NOT NULL,
    [createdDate]     DATETIME     NOT NULL,
    [createdBy]       VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ConsultantMajor] PRIMARY KEY CLUSTERED ([id] ASC)
);

