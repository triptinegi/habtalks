﻿CREATE TABLE [dbo].[user_role] (
    [id]           INT          IDENTITY (1, 1) NOT NULL,
    [roleId]       INT          NOT NULL,
    [userId]       INT          NOT NULL,
    [validFrom]    DATETIME     NOT NULL,
    [validTo]      DATETIME     NOT NULL,
    [modifiedDate] DATETIME     NOT NULL,
    [modifiedBy]   VARCHAR (50) NOT NULL,
    [createdDate]  DATETIME     NOT NULL,
    [createdBy]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_user_role] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_user_role_role] FOREIGN KEY ([roleId]) REFERENCES [dbo].[role] ([id]),
    CONSTRAINT [FK_user_role_user] FOREIGN KEY ([userId]) REFERENCES [dbo].[user] ([id])
);

