﻿CREATE TABLE [dbo].[usergroup] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [name]         VARCHAR (100) NOT NULL,
    [code]         INT           NULL,
    [description]  VARCHAR (100) NULL,
    [modifiedDate] DATETIME      NOT NULL,
    [modifiedBy]   VARCHAR (50)  NOT NULL,
    [createdDate]  DATETIME      NOT NULL,
    [createdBy]    VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_usergroup] PRIMARY KEY CLUSTERED ([id] ASC)
);

