﻿CREATE TABLE [dbo].[ClientPainArea] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [ClientId] INT NOT NULL,
    [PainId]   INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClientBooking_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientBooking] ([Id]),
    CONSTRAINT [FK_ClientBooking_PainId] FOREIGN KEY ([PainId]) REFERENCES [dbo].[PainArea] ([Id])
);

