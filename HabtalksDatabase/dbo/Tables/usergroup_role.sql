﻿CREATE TABLE [dbo].[usergroup_role] (
    [id]           INT          IDENTITY (1, 1) NOT NULL,
    [roleId]       INT          NOT NULL,
    [usergroupId]  INT          NOT NULL,
    [modifiedDate] DATETIME     NOT NULL,
    [modifiedBy]   VARCHAR (50) NOT NULL,
    [createdDate]  DATETIME     NOT NULL,
    [createdBy]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_usergroup_role] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_usergroup_role_role] FOREIGN KEY ([roleId]) REFERENCES [dbo].[role] ([id]),
    CONSTRAINT [FK_usergroup_role_usergroup] FOREIGN KEY ([usergroupId]) REFERENCES [dbo].[usergroup] ([id]),
    CONSTRAINT [IX_usergroup_role] UNIQUE NONCLUSTERED ([usergroupId] ASC, [roleId] ASC)
);

