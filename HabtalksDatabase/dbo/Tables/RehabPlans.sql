﻿CREATE TABLE [dbo].[RehabPlans] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [PlanName]        VARCHAR (25)  NOT NULL,
    [PlanDescription] VARCHAR (250) NOT NULL,
    [PlanCharges]     DECIMAL (18)  NOT NULL,
    [IsSelected]      BIT           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

