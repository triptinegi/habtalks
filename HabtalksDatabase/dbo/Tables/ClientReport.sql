﻿CREATE TABLE [dbo].[ClientReport] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [ClientId]   INT          NOT NULL,
    [ReportName] VARCHAR (20) NOT NULL,
    [ReportType] VARCHAR (10) NOT NULL,
    [Report]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__ClientRe__3214EC07705ED6DF] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClientBooking_Report] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[ClientBooking] ([Id])
);

