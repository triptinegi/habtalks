﻿CREATE TABLE [dbo].[ConsultantReview] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [ConsultantId] INT           NOT NULL,
    [Review]       VARCHAR (500) NOT NULL,
    [Rating]       INT           NOT NULL,
    [RatingBy]     INT           NOT NULL,
    [createdDate]  DATETIME      NOT NULL,
    [createdBy]    VARCHAR (50)  NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Consultant_ConsultantReview] FOREIGN KEY ([ConsultantId]) REFERENCES [dbo].[Consultant] ([id]),
    CONSTRAINT [FK_Consultant_RatingBy] FOREIGN KEY ([RatingBy]) REFERENCES [dbo].[Customer] ([id])
);

