﻿CREATE TABLE [dbo].[ClientBooking] (
    [Id]            INT          IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (50) NOT NULL,
    [DOB]           DATETIME     NOT NULL,
    [Gender]        CHAR (1)     NOT NULL,
    [Mobile]        VARCHAR (12) NOT NULL,
    [Email]         VARCHAR (35) NOT NULL,
    [Weight]        INT          NOT NULL,
    [WeightMeasure] VARCHAR (50) NULL,
    [Height]        INT          NOT NULL,
    [HeightMeasure] VARCHAR (50) NULL,
    [ActivityLevel] VARCHAR (10) NOT NULL,
    [Profession]    VARCHAR (20) NOT NULL,
    [Consent]       BIT          NOT NULL,
    CONSTRAINT [PK__ClientBo__3214EC073CAD72F8] PRIMARY KEY CLUSTERED ([Id] ASC)
);

