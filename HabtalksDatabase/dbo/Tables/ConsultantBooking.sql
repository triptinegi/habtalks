﻿CREATE TABLE [dbo].[ConsultantBooking] (
    [id]                    INT          IDENTITY (1, 1) NOT NULL,
    [ConsultantId]          INT          NOT NULL,
    [ConsultantTotalSlots]  INT          NOT NULL,
    [ConsultantBookedSlots] INT          NOT NULL,
    [modifiedDate]          DATETIME     NOT NULL,
    [modifiedBy]            VARCHAR (50) NOT NULL,
    [createdDate]           DATETIME     NOT NULL,
    [createdBy]             VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ConsultantBooking] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Consultant_ConsultantBooking] FOREIGN KEY ([ConsultantId]) REFERENCES [dbo].[Consultant] ([id])
);

