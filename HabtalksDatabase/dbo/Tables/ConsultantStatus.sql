﻿CREATE TABLE [dbo].[ConsultantStatus] (
    [id]           INT          IDENTITY (1, 1) NOT NULL,
    [Status]       VARCHAR (50) NOT NULL,
    [modifiedDate] DATETIME     NOT NULL,
    [modifiedBy]   VARCHAR (50) NOT NULL,
    [createdDate]  DATETIME     NOT NULL,
    [createdBy]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ConsultantStatus] PRIMARY KEY CLUSTERED ([id] ASC)
);

