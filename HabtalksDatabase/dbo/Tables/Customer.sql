﻿CREATE TABLE [dbo].[Customer] (
    [id]                 INT           IDENTITY (1, 1) NOT NULL,
    [firstName]          VARCHAR (100) NOT NULL,
    [lastName]           VARCHAR (100) NOT NULL,
    [phone]              VARCHAR (50)  NULL,
    [email]              VARCHAR (100) NULL,
    [description]        VARCHAR (500) NULL,
    [CustomerStatusId]   INT           NOT NULL,
    [userId]             INT           NULL,
    [modifiedDate]       DATETIME      NOT NULL,
    [modifiedBy]         VARCHAR (50)  NOT NULL,
    [createdDate]        DATETIME      NOT NULL,
    [createdBy]          VARCHAR (50)  NOT NULL,
    [statusModifiedDate] DATETIME      NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Customer_Customerstatus] FOREIGN KEY ([CustomerStatusId]) REFERENCES [dbo].[UserStatus] ([id]),
    CONSTRAINT [FK_Customer_user] FOREIGN KEY ([userId]) REFERENCES [dbo].[user] ([id])
);

