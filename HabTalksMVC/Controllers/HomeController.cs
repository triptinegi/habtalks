﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HabTalksMVC.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.Title = "";
			return View();
		}

		public ActionResult Story()
		{
			ViewBag.Title = "Story";
			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Title = "Contact Us";
			return View();
		}
		//public ActionResult Physiotherapy()
		//{
		//	ViewBag.Title = "Physiotherapy";
		//	return View();
		//}

		public ActionResult Testimonials()
		{
			ViewBag.Title = "Testimonials";
			return View();
		}
		public ActionResult Blog()
		{
			ViewBag.Title = "Blog";
			return View();
		}

		public ActionResult TermsAndConditions()
		{
			ViewBag.Title = "Terms & Conditions";
			return View();
		}

		public ActionResult PrivacyPolicy()
		{
			ViewBag.Title = "Privacy Policy";
			return View();
		}

		public ActionResult TermsOfUse()
		{
			ViewBag.Title = "Terms Of Use";
			return View();
		}

		public ActionResult Service()
		{
			ViewBag.Title = "Service";
			return View();
		}

		public ActionResult emptyTestView()
		{
			return View();
		}
	}
}