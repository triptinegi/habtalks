﻿using Core;
using HabTalksMVC.Models;
using HabTalksMVC.Utilities;
using log4net;
using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Configuration;
using System.Web.Mvc;

namespace HabTalksMVC.Controllers
{
    public class RegisterController : Controller
    {
        public ILog log = log4net.LogManager.GetLogger(typeof(RegisterController));

        // GET: Register
        public ActionResult Index()
        {
            MainPageModel main = new MainPageModel();
            using (var context = new HabTalksEntities())
            {
                main.painarea = context.PainAreas.ToList();
            }
            return View(main);
        }

        [HttpPost]
        public ActionResult Schedule(MainPageModel booking)
        {
            bool isError = false;
            bool isImageSave = false;
            string FileName = string.Empty;

            Appointment Newappointment = new Appointment();
            var payment = new PaymentModel();
            try
            {
                var ISTCurrentDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                log.Warn(ISTCurrentDateTime);

                TimeSpan CurrentTimeSpan = ISTCurrentDateTime.TimeOfDay;

                string strAppointmentIntervalInMinutes = WebConfigurationManager.AppSettings["AppointmentInterval"];
                int AppointmentIntervalInMinutes = Convert.ToInt32(strAppointmentIntervalInMinutes);

                if (ModelState.IsValid)
                {
                    DateTime timespan = DateTime.Parse(booking.Time);  //gives us a DateTime object
                    TimeSpan SelectedTime = timespan.TimeOfDay;  //returns a TimeSpan from the Time portion

                    DateTime SelectedAppointmentDate = Convert.ToDateTime(booking.AppointmentDate);

                    // first compare date
                    if (ISTCurrentDateTime.Date == SelectedAppointmentDate.Date)
                    {
                        // Get Difference between times
                        TimeSpan difference = SelectedTime.Subtract(CurrentTimeSpan);

                        if (difference.TotalMinutes <= AppointmentIntervalInMinutes)
                        {
                            ViewBag.ErrorMessage = "Can not book appointment from current time upto 3 hours, please choose different time slot";
                            return View("Index", booking);
                        }
                    }

                    // check that 2 appointment is book or not if not book then proceed otherwise give a message
                    using (var context = new HabTalksEntities())
                    {
                        int TotalAppointmentBooked = context.ClientAppointments.Where(x => 
                        DbFunctions.TruncateTime(x.AppointmentDate) == DbFunctions.TruncateTime(SelectedAppointmentDate) &&
                        x.Time == SelectedTime).Count();

                        if (TotalAppointmentBooked >= 2)
                        {
                            ViewBag.ErrorMessage = "All available bookings for time slot are already done, please choose different time slot";
                            return View("Index", booking);
                        }
                    }

                    if (booking.painarea.Where(x => x.IsSelected == true).Count() == 0)
                    {
                        ModelState.AddModelError("PainAreaError", "Select atleast one pain area");
                        return View("Index", booking);
                    }
                    if (booking.painarea.Where(x => x.IsSelected == true).Count() > 1)
                    {
                        ModelState.AddModelError("PainAreaError", "Select only one pain area");
                        return View("Index", booking);
                    }

                    if (booking.ReportFile != null && booking.ReportFile.ContentLength > 0)
                    {
                        try
                        {
                            string FileExtension = Path.GetExtension(booking.ReportFile.FileName).ToLower();

                            if (FileExtension == ".png" || FileExtension == ".jpg" || FileExtension == ".jpeg" || FileExtension == ".pdf")
                            {
                                if (booking.ReportFile.ContentLength > 3000000)
                                {
                                    ModelState.AddModelError("ReportFileError", "Allow file only upto 3MB");
                                    ViewBag.ErrorMessage = "File size limit exceed";
                                    return View("Index", booking);
                                }
                                else
                                {
                                    if (!Directory.Exists(Server.MapPath("~/" + GlobalClass.UploadReportImage)))
                                        Directory.CreateDirectory(Server.MapPath("~/" + GlobalClass.UploadReportImage));

                                    Guid g = Guid.NewGuid();
                                    FileName = g.ToString() + Path.GetExtension(booking.ReportFile.FileName);

                                    string FullPath = Path.Combine(Server.MapPath("~/" + GlobalClass.UploadReportImage), FileName);
                                    booking.ReportFile.SaveAs(FullPath);
                                    isImageSave = true;
                                }
                            }
                            else
                            {
                                ModelState.AddModelError("ReportFileError", "Upload only png, jpg, jpeg or pdf file");
                                ViewBag.ErrorMessage = "Please upload valid report file";
                                return View("Index", booking);
                            }
                        }
                        catch (Exception ex)
                        {
                            isImageSave = false;
                        }
                    }

                    using (var context = new HabTalksEntities())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                var NewBooking = new ClientBooking()
                                {
                                    Name = booking.ClientBookingModel.Name,
                                    DOB = Convert.ToDateTime(booking.ClientBookingModel.DOB),
                                    Gender = booking.ClientBookingModel.Gender,
                                    Mobile = booking.ClientBookingModel.CountryCode + booking.ClientBookingModel.Mobile,
                                    Email = booking.ClientBookingModel.Email,
                                    Weight = booking.ClientBookingModel.Weight,
                                    Height = booking.ClientBookingModel.Height,
                                    ActivityLevel = booking.ClientBookingModel.ActivityLevel,
                                    Profession = booking.ClientBookingModel.Profession,
                                    Consent = true, //booking.ClientBookingModel.Consent,

                                    WeightMeasure = booking.ClientBookingModel.WeightMeasure,
                                    HeightMeasure = booking.ClientBookingModel.HeightMeasure
                                };
                                context.ClientBookings.Add(NewBooking);
                                context.SaveChanges();

                                var LastId = NewBooking.Id;

                                payment.ClientId = LastId;

                                var newApointment = new ClientAppointment()
                                {
                                    AppointmentDate = SelectedAppointmentDate,
                                    Time = SelectedTime,                                    
                                    ClientId = LastId
                                };
                                context.ClientAppointments.Add(newApointment);
                                context.SaveChanges();

                                Newappointment.ClientId = LastId;

                                foreach (var item in booking.painarea)
                                {
                                    var clientpain = new ClientPainArea
                                    {
                                        ClientId = LastId,
                                        PainId = item.Id
                                    };
                                    if (item.IsSelected)
                                    {
                                        context.ClientPainAreas.Add(clientpain);
                                    }
                                }

                                context.SaveChanges();

                                //Save image if not empty
                                if (!string.IsNullOrEmpty(FileName) && isImageSave == true)
                                {
                                    var clientReport = new ClientReport()
                                    {
                                        ClientId = LastId,
                                        ReportName = "",
                                        ReportType = "",
                                        Report = FileName
                                    };
                                    context.ClientReports.Add(clientReport);
                                    context.SaveChanges();
                                }
                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex1)
                            {
                                dbContextTransaction.Rollback();
                                isError = true;
                                ViewBag.ErrorMessage = "An error occured while registering your appointment";
                            }
                        }
                    }
                    if (isError == false)
                    {
                        payment.UserName = booking.ClientBookingModel.Name;
                        payment.UserEmail = booking.ClientBookingModel.Email;
                        payment.UserContact = booking.ClientBookingModel.Mobile;
                        using (var context = new HabTalksEntities())
                        {
                            payment.rehabPlan = (from Plan in context.RehabPlans select Plan).ToList();
                        }
                        return View("PlanSelect", payment);
                    }
                }
                else
                    ViewBag.ErrorMessage = "Please complete all the details";
            }
            catch (Exception ex)
            {
            }
            return View("Index", booking);
        }

        [HttpPost]
        public ActionResult StartPayment(PaymentModel payment)
        {
            var pay = new PaymentModel();
            if (payment != null)
            {
                using (var context = new HabTalksEntities())
                {
                    pay.PlanCharges = (from Plan in context.RehabPlans
                                  .Where(c => c.Id == payment.SelectedPlan)
                                       select Plan.PlanCharges).FirstOrDefault();
                }
                pay.OrderId = InitiatePayment(pay.PlanCharges);
                pay.UserContact = payment.UserContact;
                pay.UserName = payment.UserName;
                pay.UserEmail = payment.UserEmail;
                pay.SelectedPlan = payment.SelectedPlan;
            }
            return View("Payment", pay);
        }

        private string InitiatePayment(decimal amount)
        {
            string orderId;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("amount", amount * 100); // this amount should be same as transaction amount
            input.Add("currency", "INR");
            input.Add("receipt", Guid.NewGuid());
            input.Add("payment_capture", 1);

            string key = GlobalClass.RazorPayApiKey;
            string secret = GlobalClass.RazorPaySecretKey;

            RazorpayClient client = new RazorpayClient(key, secret);

            Order order = client.Order.Create(input);
            orderId = order["id"].ToString();
            PaymentModel pay = new PaymentModel();
            return orderId;
        }

        public ActionResult Payment(PaymentModel payment)
        {
            return View(payment);
        }

        public ActionResult MakePayment(PaymentModel payment)
        {
            bool isError = false;

            string paymentId = Request.Form["razorpay_payment_id"];
            string orderId = Request.Form["razorpay_order_id"];
            string signature = Request.Form["razorpay_signature"];

            string ErrorCode = string.Empty;
            string ErrorDescription = string.Empty;
            string ErrorField = string.Empty;

            if (string.IsNullOrEmpty(orderId))
            {
                isError = true;
                //ErrorCode = HttpContext.Request.Form.Get("error[code]"); ;
                //ErrorDescription = HttpContext.Request.Form.Get("error[description]"); ;
                //ErrorField = HttpContext.Request.Form.Get("error[field]");
            }

            // Not needed
            //string key = GlobalClass.RazorPayApiKey;
            //string secret = GlobalClass.RazorPaySecretKey;
            //RazorpayClient client = new RazorpayClient(key, secret);

            try
            {
                Dictionary<string, string> attributes = new Dictionary<string, string>
                {
                    { "razorpay_payment_id", paymentId },
                    { "razorpay_order_id", orderId },
                    { "razorpay_signature", signature }
                };

                Utils.verifyPaymentSignature(attributes);
            }
            catch (Exception ex)
            {
                isError = true;
            }

            bool status;
            if (isError == false)
                status = true;

            Refund refund = new Razorpay.Api.Payment((string)paymentId).Refund();
            if (refund["id"] != null)
                status = true;
            else
                status = false;

            using (var context = new HabTalksEntities())
            {
                var clientPayment = new ClientPayment()
                {
                    ClientId = payment.ClientId,
                    PaymentDone = status,
                    razorpay_payment_id = paymentId,
                    razorpay_signature = string.IsNullOrWhiteSpace(signature) ? "" : signature,
                    SelectedPlan = payment.SelectedPlan,
                    refund_payment_id = refund["id"]
                };
                context.ClientPayments.Add(clientPayment);
                context.SaveChanges();
            }

            #region Send An Email
            if (status == true)
            {
                // Send Email To Admin
                // get ClientBooking, ClientAppointment, ClientReport, ClientPainArea data

                using (var context = new HabTalksEntities())
                {
                    ClientBooking clientBooking = context.ClientBookings.Where(c => c.Id == payment.ClientId).FirstOrDefault();
                    ClientAppointment clientAppointment = context.ClientAppointments.Where(c => c.ClientId == payment.ClientId).FirstOrDefault();
                    ClientReport clientReport = context.ClientReports.Where(c => c.ClientId == payment.ClientId).FirstOrDefault();

                    var ClientPainArea = (from cpa in context.ClientPainAreas
                                          join pa in context.PainAreas on cpa.PainId equals pa.Id
                                          where cpa.ClientId == payment.ClientId
                                          select pa.Pain_Area).ToArray();
                    string ReportFileFullPath = string.Empty;

                    if (clientReport != null)
                    {
                        if (!string.IsNullOrWhiteSpace(clientReport.Report))
                            ReportFileFullPath = Path.Combine(Server.MapPath("~/" + GlobalClass.UploadReportImage), clientReport.Report);
                    }
                    SendEmails sendEmails = new SendEmails();
                    sendEmails.SendAppointmentEmail(clientBooking, clientAppointment, ReportFileFullPath, ClientPainArea);
                }
            }
            #endregion
            return View("Confirmation", status);
        }
    }
}