﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HabTalksMVC.Utilities
{
    /// <summary>
    /// The field is required when validating on the client, but no validation is done
    /// on the server. Use this attribute for fields that are valid if they are present
    /// in the form on the client, but requires more complex validation rules on the server.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class ClientRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        /// <summary>
        /// Always returns true. This class is meant to be used with complex validation rules.
        /// </summary>
        /// <param name="value">Not used.</param>
        /// <returns>True</returns>
        public override bool IsValid(object value)
        {
            return true;
        }

        /// <summary>
        /// Gets the client validation rules used with jquery.validate.unobtrusive.
        /// </summary>
        /// <param name="metadata">Not used.</param>
        /// <param name="context">Not used.</param>
        /// <returns>Validation attributes for the required attribute.</returns>
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, 
            ControllerContext context)
        {
            yield return new ModelClientValidationRequiredRule(metadata.DisplayName + " måste anges");
        }
    }
}
