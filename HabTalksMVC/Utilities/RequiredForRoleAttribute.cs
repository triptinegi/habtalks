﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web;

namespace HabTalksMVC.Utilities
{
    /// <summary>
    /// A conditional required attribute that is enabled only for a specific user role.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class RequiredForRoleAttribute : ValidationAttribute, IClientValidatable
    {
        /// <summary>
        /// Role that this attribute applies to.
        /// </summary>
        public string Role { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="role">The role that the property is required for.</param>
        public RequiredForRoleAttribute(string role)
        {
            Role = role;
        }

        /// <summary>
        /// Checks if the property is neither null nor empty, if the user is in the specified role.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1820:TestForEmptyStringsUsingStringLength")]
        public override bool IsValid(object value)
        {
            return !HttpContext.Current.User.IsInRole(Role) ||
                (value != null && value as string != string.Empty);
        }

        /// <summary>
        /// Gets the client validation rules used with jquery.validate.unobtrusive.
        /// </summary>
        /// <param name="metadata">Not used.</param>
        /// <param name="context">Not used.</param>
        /// <returns>Validation attributes for the required attribute, if the user is in the specified role.</returns>
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, 
            ControllerContext context)
        {
            if (HttpContext.Current.User.IsInRole(Role))
            {
                yield return new ModelClientValidationRequiredRule(ErrorMessage);
            }
        }
    }
}
