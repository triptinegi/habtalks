﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace HabTalksMVC.Utilities
{
    /// <summary>
    /// Validator class for mandatory attributes
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class MandatoryAttribute : ValidationAttribute, IClientValidatable
    {
        /// <summary>
        /// Checks if parameter is valid.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Value</returns>
        public override bool IsValid(object value)
        {
            return (!(value is bool) || (bool)value);
        }

        /// <summary>
        /// Gets client validation rules.
        /// </summary>
        /// <param name="metadata"></param>
        /// <param name="context"></param>
        /// <returns>rule</returns>
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName());
            rule.ValidationType = "mandatory";
            yield return rule;
        }
    }
}