﻿using Core;
using log4net;
using System;
using System.Net.Mail;

namespace HabTalksMVC.Utilities
{
    public class SendEmails
    {
        public ILog log = log4net.LogManager.GetLogger(typeof(SendEmails));
        public bool SendAppointmentEmail(ClientBooking clientBooking, ClientAppointment clientAppointment, string ReportFullFilePath, string[] clientPainArea)
        {
            try
            {
                string EmailMsg = "<table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'><tbody>" +
                                "<tr><td><p>Hi Admin,</p><p>Please look at below Appointment Detail.</p></td></tr></tbody></table>";

                EmailMsg += "<br><table border='1' cellpadding='1' cellspacing='0' style='width:100%'><tbody>" +
                            "<tr><td>&nbsp;Name</td><td><p><b>&nbsp;" + clientBooking.Name + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Data of Birth</td><td><p><b>&nbsp;" + clientBooking.DOB.ToString("yyyy-MM-dd") + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Gender</td><td><p><b>&nbsp;" + clientBooking.Gender + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Mobile</td><td><p><b>&nbsp;" + clientBooking.Mobile + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Email</td><td><p><b>&nbsp;" + clientBooking.Email + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Weight</td><td><p><b>&nbsp;" + clientBooking.Weight + "&nbsp;" + clientBooking.WeightMeasure + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Height</td><td><p><b>&nbsp;" + clientBooking.Height + "&nbsp;" + clientBooking.HeightMeasure + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Activity</td><td><p><b>&nbsp;" + clientBooking.ActivityLevel + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Profession</td><td><p><b>&nbsp;" + clientBooking.Profession + "</b></p></td></tr>" +
                            "</tbody></table>";

                EmailMsg += "<br><table border='1' cellpadding='1' cellspacing='0' style='width:100%'><tbody>" +
                            "<tr><td>&nbsp;Appointment Date</td><td><p><b>&nbsp;" + clientAppointment.AppointmentDate.ToString("yyyy-MM-dd") + "</b></p></td></tr>" +
                            "<tr><td>&nbsp;Appointment Time</td><td><p><b>&nbsp;" + clientAppointment.Time.ToString() + "</b></p></td></tr>" +
                            "</tbody></table>";

                if (clientPainArea != null)
                {
                    if (clientPainArea.Length > 0)
                    {
                        EmailMsg += "<br><table border='0' cellpadding='0' cellspacing='0' style='width: 100%;'><tbody><tr><td><p>Pain Area</p></td></tr></tbody></table>";

                        EmailMsg += "<br><table border='1' cellpadding='1' cellspacing='0' style='width:100%'><tbody>";

                        foreach (var item in clientPainArea)
                        {
                            EmailMsg += "<tr><td><p><b>&nbsp;" + item.ToString() + "</b></p></td></tr>";
                        }

                        EmailMsg += "</tbody></table>";
                    }
                }

                var IsMailSend = SendMail("admin@habtalks.com", "HabTalks Admin", "Appointment Details", EmailMsg, ReportFullFilePath);
                return IsMailSend;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool SendMail(string ToEmailId, string ToName, string Subject, string MailText, string ReportFullFilePath)
        {
            try
            {
                SmtpClient smtp = GetSMTPSettings();
                MailMessage myMessage = new MailMessage();
                MailAddress fromMailAddress = null;

                fromMailAddress = GetFromMailAddress();
                MailAddress toMailAddress = new MailAddress(ToEmailId, ToName);
                myMessage.To.Add(toMailAddress);
                myMessage.Subject = Subject;
                myMessage.From = fromMailAddress;
                myMessage.IsBodyHtml = true;
                myMessage.Body = MailText;

                // attachment
                if (!string.IsNullOrWhiteSpace(ReportFullFilePath))
                {
                    myMessage.Attachments.Add(new Attachment(ReportFullFilePath));
                }
                smtp.Send(myMessage);
            }
            catch (Exception ex)
            {
                log.Error("Send mail error :");
                log.Error(ex);
                return false;
            }
            return true;
        }

        public static SmtpClient GetSMTPSettings()
        {
            SmtpClient smtp = new SmtpClient();
            //smtp.Host = "smtp.gmail.com";
            smtp.Host = "relay-hosting.secureserver.net";
            //smtp.Port = 587;
            smtp.Port = 25;
            smtp.EnableSsl = false;
            smtp.UseDefaultCredentials = false;
            smtp.Timeout = 2000000;
            smtp.Credentials = new System.Net.NetworkCredential("contact@habtalks.com", "kushwahaarpit");
            return smtp;
        }

        public static MailAddress GetFromMailAddress()
        {
            return (new MailAddress("contact@habtalks.com", "HabTalks"));
        }
    }
}