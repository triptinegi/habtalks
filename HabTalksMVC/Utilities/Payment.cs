﻿using Razorpay.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace HabTalksMVC.Utilities
{
	public class Payment
	{
		private static string RazorpayKey = "sfg dfgsdfdfgsdf";
		private static string RazorpaySecret = "dfg sdfgsdfg";
		public void makepayment()
		{
			#region Razorpay Call
			//Dictionary<string, object> input = new Dictionary<string, object>();
			//decimal payableAmount = objBooking.appGrandTotal.Value;
			//if (objRequest.returnBookingID != 0)
			//	payableAmount += objReturnBooking.appGrandTotal.Value;
			//input.Add("amount", (payableAmount * 100));
			//RazorpayClient client = new RazorpayClient(RazorpayKey, RazorpaySecret);
			//string paymentId = objRequest.transactionID.ToString();
			//Razorpay.Api.Payment payment = client.Payment.Fetch(paymentId);
			//Razorpay.Api.Payment capturedPayment = payment.Capture(input);
			//try
			//{
			//	string paymentId = objRequest.transactionID.ToString();
			//	decimal payableAmount = objBooking.appGrandTotal.Value;
			//	if (objRequest.returnBookingID != 0)
			//		payableAmount += objReturnBooking.appGrandTotal.Value;
			//	string razorPay = appGlobalVars.RazorpayPostPaymentURL + paymentId + "/capture";
			//	RazorpayPaymentRequest objRazorpayRequest = new RazorpayPaymentRequest()
			//	{
			//		amount = payableAmount * 100,
			//	};
			//	var responseDetails = InvokePostRazorpayInvoiceMethod(objRazorpayRequest, razorPay);

			//}
			//catch (Exception ex)
			//{
			//	throw ex;
			//}
			#endregion
		}
		public static string InvokePostRazorpayInvoiceMethod(object obj, string url)
		{
			try
			{

				//string UrlPrefix = System.Web.Configuration.WebConfigurationManager.AppSettings["RazorpayInvoiceURL"].ToString();
				//string UrlPrefix = appGlobalVars.RazorpayPaymentURL;
				//ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
				HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

				//var request = (HttpWebRequest)WebRequest.Create(UrlPrefix);

				request.ContentType = "application/json";
				request.Method = "POST";

				string credentials = string.Format("{0}:{1}", RazorpayKey, RazorpaySecret);
				// Not my real API KEY....
				byte[] bytes = Encoding.ASCII.GetBytes(credentials);
				string base64 = Convert.ToBase64String(bytes);
				string authorization = String.Concat("Basic ", base64);

				//request.UseDefaultCredentials = true; 
				request.Headers.Add("Authorization", authorization);

				//request.Headers.Add("Authorization", strAuthToken);
				//Do no upload on live : 
				//Only for test enviroment : Solve error of Could not create SSL/TLS secure channel.
				//ServicePointManager.Expect100Continue = true;
				//ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
				//end

				using (var streamWriter = new StreamWriter(request.GetRequestStream()))
				{
					string json = new JavaScriptSerializer().Serialize(obj);
					streamWriter.Write(json);

				}

				var response = (HttpWebResponse)request.GetResponse();
				var streamReader = new StreamReader(response.GetResponseStream());
				return streamReader.ReadToEnd();
			}
			catch (WebException wex)
			{
				if (wex.Response != null)
				{
					using (var errorResponse = (HttpWebResponse)wex.Response)
					{
						using (var reader = new StreamReader(errorResponse.GetResponseStream()))
						{
							return reader.ReadToEnd();                     //TODO: use JSON.net to parse this string and look at the error message                
						}
					}
				}
			}
			return string.Empty;
		}
	}
}