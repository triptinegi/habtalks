﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace HabTalksMVC.Utilities
{
	/// <summary>
	/// Form html helper methods.
	/// </summary>
	public static class NewFormExtensions
	{
		/// <summary>
		/// Method that determines if the attribute is required or not
		/// </summary>
		/// <param name="metaData">metaData</param>
		/// <returns>true/false</returns>
		private static bool MarkAsMandatory(ModelMetadata metaData)
		{
			var requiredAttributes = new List<Type> {
				typeof(ClientRequiredAttribute),
				typeof(MandatoryAttribute),
				typeof(RequiredForRoleAttribute)
			};

			if (metaData.ModelType != typeof(bool))
			{
				requiredAttributes.Add(typeof(RequiredAttribute));
			}

			var property = metaData.ContainerType.GetProperty(metaData.PropertyName);

			return property.GetCustomAttributes(true).Select(ca => ca.GetType())
				.Where(cat => requiredAttributes.Contains(cat)).Any();
		}

		/// <summary>
		/// Method that returns text for required star attribute
		/// </summary>
		/// <typeparam name="TModel">model</typeparam>
		/// <param name="html">not used</param>
		/// <returns></returns>
		public static MvcHtmlString RequiredStar<TModel>(this HtmlHelper<TModel> html)
		{
			return MvcHtmlString.Create("This field is required");
		}

		/// <summary>
		/// Html helper to produce a div with a label and editor field inside.
		/// </summary>
		/// <typeparam name="TModel">Type of the model.</typeparam>
		/// <typeparam name="TField">Type of the field.</typeparam>
		/// <param name="html">Html helper object to extend.</param>
		/// <param name="expression">An expression that identifies the field to create an entry for.</param>
		/// <param name="options">An object containing html attributes to be set for the input element</param>
		/// <param name="appendedFields">Additional HTML data</param>
		/// <returns>MvcHtmlString.</returns>
		public static MvcHtmlString EditorEntryFor<TModel, TField>(this HtmlHelper<TModel> html,
			Expression<Func<TModel, TField>> expression, object options, params MvcHtmlString[] appendedFields)
		{
			var requiredStar = new TagBuilder("span");
			ModelMetadata data = ModelMetadata.FromLambdaExpression<TModel, TField>(expression, html.ViewData);

			if (MarkAsMandatory(data))
			{
				requiredStar.SetInnerText("*");
				requiredStar.AddCssClass("requiredstar");
				requiredStar.Attributes["title"] = html.RequiredStar().ToString();
			}

			MvcHtmlString editor = GetRegularOrEnumEditor<TModel, TField>(html, expression, options, data);
			return BuildFormEntry(MvcHtmlString.Create(html.LabelFor(expression).ToString() + requiredStar.ToString()).ToHtmlString(),
				editor.ToHtmlString(),
				html.ValidationMessageFor(expression).ToHtmlString(),
				appendedFields: appendedFields.Select(f => f.ToHtmlString()).ToArray());
		}

		private static MvcHtmlString BuildFormEntry(string labelSection, string dataSection, string errorSection = null, bool errorLocationBelow = false, params string[] appendedFields)
		{
			var div = new TagBuilder("div");
			div.AddCssClass("row"); // Bootstrap row
			var innerHtml = new StringBuilder();
			innerHtml.Append(MakeLeftCol(labelSection));
			if (dataSection.Contains("checkbox"))
			{
				innerHtml.Append(MakeAutoCol(dataSection));
			}
			else
			{
				//The reason that code looks like this is because some editors uses an extra div to push error message down one row
				//but the problem araises when the editor is inside a row with divs. Then the error message get to the wrong location.
				//So old version is more space and new aka errorLocationBelow more correctly connected to the field that casuses the errror.
				if (errorLocationBelow)
				{
					var br = new TagBuilder("/br");
					innerHtml.Append(MakeMiddleCol(dataSection + br.ToString() + errorSection));
				}
				else
				{
					innerHtml.Append(MakeMiddleCol(dataSection));
				}
			}

			if (appendedFields.Any())
			{
				WrapAppendedFields(appendedFields, innerHtml);
			}

			if (!errorLocationBelow)
			{
				innerHtml.Append(MakeValidationCol(errorSection));
			}

			div.InnerHtml = innerHtml.ToString();
			return MvcHtmlString.Create(div.ToString());
		}

		/// <summary>
		/// Wrap appended fields and align right when presented as a Bootstrap label
		/// </summary>
		/// <param name="appendedFields"></param>
		/// <param name="innerHtml"></param>
		private static void WrapAppendedFields(string[] appendedFields, StringBuilder innerHtml)
		{
			var content = new StringBuilder();
			foreach (var appendedField in appendedFields)
			{
				content.Append(appendedField);
				content.Append(" ");
			}

			// Float the content when the appended field is a label, otherwise just render as is.
			if (appendedFields.Any(f => f.Contains("label label-")))
			{
				var label = new TagBuilder("div");
				label.AddCssClass("pull-right");
				label.InnerHtml = content.ToString();
				innerHtml.Append(label);
			}
			else
			{
				innerHtml.Append(content);
			}
		}
		private static MvcHtmlString GetRegularOrEnumEditor<TModel, TField>(HtmlHelper<TModel> html, Expression<Func<TModel, TField>> expression, object options, ModelMetadata data)
		{
			if (EnumHelper.IsValidForEnumHelper(data))
			{
				return html.EnumDropDownListFor(expression, options);
			}
			else
			{
				return html.EditorFor(expression, options);
			}
		}

		private static MvcHtmlString MakeLeftCol(string content)
		{
			return MakeCol(content, "col-left");
		}

		private static MvcHtmlString MakeMiddleCol(string content)
		{
			return MakeCol(content, "col-middle");
		}

		private static MvcHtmlString MakeAutoCol(string content)
		{
			return MakeCol(content, "col-auto-width");
		}

		private static MvcHtmlString MakeValidationCol(string content)
		{
			return MakeCol(content, "col-validation");
		}

		private static MvcHtmlString MakeCol(string content, string bootstrapClass)
		{
			var div = new TagBuilder("div");
			div.AddCssClass(bootstrapClass); // Bootstrap col
			div.InnerHtml = content;
			return MvcHtmlString.Create(div.ToString());
		}

	}
}