﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HabTalksMVC.Startup))]
namespace HabTalksMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
