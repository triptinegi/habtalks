﻿$(function () {

    $('#painAreaOneErrorMsg').hide();
    $('#painAreaMorethenOneErrorMsg').hide();

    $("#frmRegister").submit(function (e) {
        // note that it's better to use form Id to select form
        var painAreaSelect = [];
        $.each($(".painAreaClass:checked"), function () {
            painAreaSelect.push($(this).val());
        });

        if (painAreaSelect.length == 0) {
            $('#painAreaOneErrorMsg').show();
            $('#painAreaMorethenOneErrorMsg').hide();            
            e.preventDefault();
        }
        else if (painAreaSelect.length > 1) {
            $('#painAreaOneErrorMsg').hide();
            $('#painAreaMorethenOneErrorMsg').show();            
            e.preventDefault();
        }
        else {
            $('#painAreaOneErrorMsg').hide();
            $('#painAreaMorethenOneErrorMsg').hide();
        }
    });
    setNavigation();

    //$(this).find("#finalize").attr('disabled', true);

    //$(this).find("#ClientBookingModel_Consent").click(function () {
    //    if ($(this).is(":checked")) {
    //        $("#finalize").attr('disabled', false);
    //    }
    //    else {
    //        $("#finalize").attr('disabled', true);
    //    }
    //});

});

function setNavigation() {
    var path = window.location.pathname;
    //path = path.replace(/\/$/, "");
    //path = decodeURIComponent(path);

    $("#mainav a").each(function () {
        var href = $(this).attr('href');
        if (path == '/Home/Index') {
            $('ul li:first').addClass('active');
        }
        else if (path == href) {
            $(this).closest('li').addClass('active');
        }
    });
}