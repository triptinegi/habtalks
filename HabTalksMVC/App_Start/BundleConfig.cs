﻿using System.Web;
using System.Web.Optimization;

namespace HabTalksMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //            "~/Scripts/jquery-{version}.js",
            //            "~/Scripts/bootstrap.js"
            //            ));
            //bundles.Add(new ScriptBundle("~/bundles/Common").Include(
            //            "~/Scripts/Common.js"
            //            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                       "~/Content/js/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Content/js/jquery.min.js",
                "~/Content/js/bootstrap.min.js",
                "~/Content/js/nav.js",
                "~/Content/js/animsition.js",
                "~/Content/js/animsition-script.js",
                "~/Content/js/jquery.sticky.js",
                "~/Content/js/sticky-header.js",
                "~/Content/js/back-to-top.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/Content/css/bootstrap.min.css",
                "~/Content/css/style.css",
                "~/Content/css/animsition.min.css",

                "~/Content/css/font-awesome.min.css",
                "~/Content/css/slick.css",

                "~/Content/css/slick-theme.css",
                "~/Content/css/fontello.css"
            ));
        }
    }
}