﻿$(function () {

    $(this).find("#finalize").attr('disabled', true);

    $(this).find("#clientModel_Consent").click(function () {
        if ($(this).is(":checked")) {
            $("#finalize").attr('disabled', false);
        }
        else {
            $("#finalize").attr('disabled', true);
        }
    });
    setNavigation();
});

function setNavigation() {
    var path = window.location.pathname;
    //path = path.replace(/\/$/, "");
    //path = decodeURIComponent(path);

    $("#mainav a").each(function () {
        var href = $(this).attr('href');
        if (path == '/Home/Index') {
            $('ul li:first').addClass('active');
        }
        else if (path == href) {
            $(this).closest('li').addClass('active');
        }
    });
}