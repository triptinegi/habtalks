$(document).ready(function () {
    $('#painAreaOneErrorMsg').hide();
    $('#painAreaMorethenOneErrorMsg').hide();

    $('#datetimepicker1').datetimepicker({
        maxDate: new Date(),
        useCurrent: false,
        format: 'DD/MM/YYYY'
    });

    $('#datetimepickerDate').datetimepicker({
        minDate: new Date(),
        useCurrent: true,
        format: 'DD/MM/YYYY'
    });

    //$('#datetimepickerTime').datetimepicker({
    //    format: 'LT',        
    //    disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 19, 20, 21, 22, 23, 24],
    //    enabledHours: [9, 10, 11, 12, 13, 14, 15, 16, 17, 18]        
    //});

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
        allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        if (curStepBtn == 'step-1') {
            var painAreaSelect = [];
            $.each($(".painAreaClass:checked"), function () {
                painAreaSelect.push($(this).val());
            });

            if (painAreaSelect.length == 0) {
                isValid = false;
                $('#painAreaOneErrorMsg').show();
                $('#painAreaMorethenOneErrorMsg').hide();
                //e.preventDefault();
            }
            else if (painAreaSelect.length > 1) {
                isValid = false;
                $('#painAreaOneErrorMsg').hide();
                $('#painAreaMorethenOneErrorMsg').show();
                //e.preventDefault();
            }
            else {
                $('#painAreaOneErrorMsg').hide();
                $('#painAreaMorethenOneErrorMsg').hide();
            }
        }
        else {
            // for step 2 and 3
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        }
        if (isValid) {
            if (curStepBtn == 'step-3') {
                $('#painAreaOneErrorMsg').hide();
                $('#painAreaMorethenOneErrorMsg').hide();                
                $('#frmRegister').submit();
            }
            else {
                nextStepWizard.removeAttr('disabled').trigger('click');
            }
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

        $(".form-group").removeClass("has-error");
        prevStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
