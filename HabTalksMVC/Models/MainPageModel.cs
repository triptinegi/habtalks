﻿using Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace HabTalksMVC.Models
{
    public enum ActivityLevel
    {
        Active,
        Athletic,
        Minimal,
        Moderate
    }
    public class MainPageModel
    {
        public ActivityLevel activity { get; set; }
        [Required(ErrorMessage = "Time is required")]
        public string Time { get; set; }
        [Required(ErrorMessage = "Appointment date is required")]
        public string AppointmentDate { get; set; }
        public HttpPostedFileBase ReportFile { get; set; }
        public List<PainArea> painarea { get; set; }
        //public ClientBooking clientModel { get; set; }
        public ClientBookingModel ClientBookingModel { get; set; }
        public ClientAppointment appointment { get; set; }

    }

    public class Appointment
    {
        public ClientAppointment appointment { get; set; }
        public int ClientId { get; set; }
    }
    public class PaymentModel
    {
        public int SelectedPlan { get; set; }
        public decimal PlanCharges { get; set; }
        public List<RehabPlan> rehabPlan { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserContact { get; set; }

        public string OrderId;
        public int ClientId { get; set; }
    }

    public class ClientBookingModel
    {
        //public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "{0} must be only {1} character long.")]
        public string Name { get; set; }
        [Required]
        public string DOB { get; set; }
        [Required]
        public string Gender { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "{0} must be {1} digit long.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "CountryCode must be numeric")]
        public string CountryCode { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "{0} must be only {1} digit long.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Mobile number must be numeric")]
        public string Mobile { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(35, ErrorMessage = "{0} must be only {1} character long.")]
        public string Email { get; set; }
        [Required]
        public int Weight { get; set; }
        [Required]
        public int Height { get; set; }
        [Required]
        public string ActivityLevel { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "{0} must be only {1} character long.")]
        public string Profession { get; set; }
        //[Required]
        public bool Consent { get; set; }
        [Required]
        public string WeightMeasure { get; set; }
        [Required]
        public string HeightMeasure { get; set; }
    }
}